#!/usr/bin/env nu

glob **/Dockerfile*  | each { |file|
  const pwd  = path self .
  let split = $file | split row '/'
  let image = ($file | path dirname | path relative-to $pwd)
  let dockerfile = ($file | split row '/'| last)
  let tag = ($dockerfile|split row . | if ($in|length) > 1 {$"-($in.1)"} else {''})
  {
    $"($image)($tag)": {
      extends: ".build"
      variables: {
        # test:       ($file | path relative-to $pwd)
        CONTEXT:    $"${CI_PROJECT_DIR}/($image)"
        DOCKERFILE: ($dockerfile)
        IMAGE:      $"${CI_REGISTRY_IMAGE}/($image):${CI_COMMIT_REF_NAME}($tag)"
      }
    }
  }
}
| reduce {|it| merge $it}
| to yaml 
