local job(context, dockerfile, path, tag) =
{
    extends: '.build',
    variables: {
      CONTEXT: '${CI_PROJECT_DIR}/'+context,
      DOCKERFILE: dockerfile,
      DESTINATION: '${CI_REGISTRY_IMAGE}/'+context+':${CI_COMMIT_REF_NAME}'+tag,
    },
};

local files = std.split(std.extVar('files'), ',');
local list = [
  {
    path:       x,
    context:    std.split(x,'/')[1],
    local dockerfile = std.split(x,'/')[2],
    dockerfile: dockerfile,
    tag: if std.startsWith(dockerfile, 'Dockerfile.') then '-'+std.split(dockerfile,'Dockerfile.')[1] else '',
  }
  for x in files
];


std.manifestYamlDoc(
  // {
  //   default: {
  //     tags: std.split(std.extVar('TAGS'),','),
  //   }
  // }+
  {
    [ x['context']+x['tag'] ]: 
    job(x['context'], x['dockerfile'], x['path'], x['tag'])
    for x in list
  },
  indent_array_in_object=true,
  quote_keys=false,
)
